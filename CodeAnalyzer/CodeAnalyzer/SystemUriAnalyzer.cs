using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using CodeAnalyzer.Utility;

namespace CodeAnalyzer
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public class SystemUriAnalyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "SystemUriAnalyzer";

        // You can change these strings in the Resources.resx file. If you do not want your analyzer to be localize-able, you can use regular strings for Title and MessageFormat.
        // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/Localizing%20Analyzers.md for more on localization
        private static readonly LocalizableString Title = new LocalizableResourceString(nameof(Resources.SystemUriAnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString MessageFormat = new LocalizableResourceString(nameof(Resources.SystemUriAnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Description = new LocalizableResourceString(nameof(Resources.SystemUriAnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private const string Category = "Deprecated Methods";

        private static DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(Rule); } }

        public override void Initialize(AnalysisContext context)
        {
            // TODO: Consider registering other actions that act on syntax instead of or in addition to symbols
            // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/Analyzer%20Actions%20Semantics.md for more information
            //context.RegisterSyntaxNodeAction(AnalyzeSymbol, SyntaxKind.SimpleAssignmentExpression, SyntaxKind.VariableDeclaration, SyntaxKind.ReturnStatement, SyntaxKind.Argument);
            context.RegisterSyntaxNodeAction(AnalyzeSymbol, SyntaxKind.InvocationExpression);
        }

        private static void AnalyzeSymbol(SyntaxNodeAnalysisContext context)
        {
            InvocationExpressionSyntax method = context.Node as InvocationExpressionSyntax;

            if (method == null)
            {
                return;
            }

            var deprecatedMethods = new List<string>()
            {
                "EscapeUriString",
                "EscapeDataString"
            };


            var memberAccessExpr = method?.Expression as MemberAccessExpressionSyntax;
            // TODO: 
            if (deprecatedMethods.Contains(memberAccessExpr?.Name.ToString()))
            {
                context.ReportDiagnostic(Diagnostic.Create(Rule, context.Node.GetLocation(), method.Expression.ToString()));
            }            
        }

        //private static void AnalyzeSymbol(SyntaxNodeAnalysisContext context)
        //{
        //    InvocationExpressionSyntax invocationExpr = InvocationExpressionExtractor.Extract(context.Node);


        //    if (invocationExpr == null)
        //    {
        //        return;
        //    }

        //    var memberAccessExpr = invocationExpr?.Expression as MemberAccessExpressionSyntax;
        //    if (memberAccessExpr?.Name.ToString() != "EscapeUriString")
        //    {
        //        return;
        //    }            

        //    //// TODO: Replace the following code with your own analysis, generating Diagnostic objects for any issues you find
        //    //var methodSymbol = (IMethodSymbol)context.Symbol;

        //    //// Find just those named type symbols with names containing lowercase letters.
        //    //if (methodSymbol.Name == "EscapeUriString")
        //    //{
        //    //    // For all such symbols, produce a diagnostic.
        //    //    var diagnostic = Diagnostic.Create(Rule, methodSymbol.Locations[0], methodSymbol.Name);

        //    //    context.ReportDiagnostic(diagnostic);
        //    //}

        //    context.ReportDiagnostic(Diagnostic.Create(Rule, context.Node.GetLocation(), invocationExpr.Expression.ToString()));
        //}
    }
}
